CC=GOARCH=amd64 go build
BIN=inspiration
SRC=./inspiration.go
ENV_WINDOWS=GOOS=windows
REL_WINDOWS=windows
OUT_WINDOWS=build/windows/
ENV_LINUX=GOOS=linux 
REL_LINUX=linux
OUT_LINUX=build/linux/
ENV_MACOS=GOOS=darwin
REL_MACOS=darwin
OUT_MACOS=build/macos/

all: windows macos linux

windows: 
	$(ENV_WINDOWS) $(CC) $(SRC)
	mkdir -p $(OUT_WINDOWS)
	mv $(BIN).exe $(OUT_WINDOWS)/$(BIN)_windows_amd64.exe

linux:
	$(ENV_LINUX) $(CC) $(SRC)
	mkdir -p $(OUT_LINUX)
	mv $(BIN) $(OUT_LINUX)/$(BIN)_linux_amd64

macos:
	$(ENV_MACOS) $(CC) $(SRC)
	mkdir -p $(OUT_MACOS)
	mv $(BIN) $(OUT_MACOS)/$(BIN)_macos_amd64

.PHONY: all windows macos linux
