package main

import (
	"fmt"
)

func main() {
	fmt.Println(`
        ____
        (.   \
          \  |  
           \ |___(\--/)
         __/    (  . . )
        "'._.    '-.O.'
             '-.  \ "|\
                '.,,/'.,,  Don't let your dreams be dreams`)
}
