# Inspirational Kitten

This includes a Makefile and options for building MacOS, Windows, and Linux

```sh
make $platform
```

Or if you simply want to inspire any and all your friends, run all the builders and anyone on any platform can enjoy!

```sh
make all
# sorry TempleOS fans. No love for you
```
